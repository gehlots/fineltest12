#!/usr/bin/env bash
#!/bin/bash

set -xe
cd /var/www/html
ls -la /var/www > /home/ec2-user/platformHook.txt
sudo composer.phar install
